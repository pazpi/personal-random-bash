if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty2 ]]; then
  export QT_QPA_PLATFORM=wayland
  XDG_SESSION_TYPE=wayland exec dbus-run-session gnome-session
fi
