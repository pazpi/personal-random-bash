#!/bin/bash

FOLDER=$1

# printf '\\begin{figure}\n\centering\n'

for img in $(find $FOLDER -type f -name "*.png" ! -name "0riginal.png"); do
    
    path=${img%.*}
    filename=$(basename $img)

    new_path=$(echo $path | sed 's/_/\\string_/g')
    new_filename=$(echo $filename | sed 's/_/\\string_/g')
    printf '\\begin{subfigure}[]{0.24\\textwidth}\n'
    printf '\\includegraphics[width=.8\\textwidth]{{%s}.png}\n' $new_path
    printf '\caption{%s}\n' ${filename:12:-4}
    printf '\\end{subfigure}\n'
    
done;

printf '\\label{fig:test}\n'
# printf '\\end{figure}\n'
