#!/bin/bash

FOLDER=$1
LD_LIBRARY_PATH=/opt/matlab/R2016b/bin/glnxa64:/opt/matlab/R2016b/runtime/glnxa64
export LD_LIBRARY_PATH

# array of wanted images
disp_img=(1 2 3 5 10 15 20)
# index for current image
i=0

for img in $(find $FOLDER -type f -name "*.png" | sort -r); do
    
    if [[ "$i" -eq "0" ]]; then
        path=$(dirname $img)
        new_path=$(echo $path | sed 's/_/\\string_/g')
        filename=$(basename $(dirname $img))
        labels=$(.././print_labels ${filename:5:-7}.tif)
        labels=$(echo $labels| perl -pe 's{,}{++$n % 2 ?  $& : "\\\\"}ge')

        printf '\\begin{subfigure}[]{0.24\\textwidth}\n'
        printf '\caption*{Query image}'
        printf '\\includegraphics[width=.8\\textwidth]{{%s/0riginal}.png}\n' $new_path
        printf '\caption{\\textit{%s}}\n' $labels
        printf '\\end{subfigure}\n'
    fi
    ((i++))
    if [[ " ${disp_img[@]} " =~ " ${i} " ]]; then

        path=${img%.*}
        filename=$(basename $img)

        new_path=$(echo $path | sed 's/_/\\string_/g')
        new_filename=$(echo $filename | sed 's/_/\\string_/g')
        labels=$(.././print_labels ${filename:12:-4}.tif)
        labels=$(echo $labels| perl -pe 's{,}{++$n % 2 ?  $& : "\\\\"}ge')

        printf '\\begin{subfigure}[]{0.24\\textwidth}\n'
        printf '\caption*{$%s^{th}$}\n' $i
        printf '\\includegraphics[width=.8\\textwidth]{{%s}.png}\n' $new_path
        printf '\caption{\\textit{%s}}\n' $labels
        printf '\\end{subfigure}\n'
    fi
done;

printf '\\label{fig:test}\n'
