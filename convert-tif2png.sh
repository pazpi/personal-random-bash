#!/bin/bash

# FOLDER=~/Projects/Tesi-Triennale/Laurea-Latex
FOLDER=$1

for img in $(find $FOLDER -type f -name *.tif); do
    filename=${img%.*}
    # new_filename=$(echo $filename | sed 's/_/-/g')
    convert "$filename.tif" "$filename.png"
    # echo $filename
done    
