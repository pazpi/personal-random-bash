#!/usr/bin/env bash

# Script to convert H264 video to the newest H265 format, saving space.
# Still testing

while IFS= read -rd '' f; do
    # http://mywiki.wooledge.org/BashFAQ/073
    FullPath=$f
    Filename=${FullPath##*/}
    PathPref=${FullPath%"$Filename"}
    # Name file without extension
    FileStub=${Filename%.*}
    FileExt=${Filename#"$FileStub"}

    if grep -q 'h265' $Filename; then
        echo $Filename " is already in h265!"
    else
        if grep -q 'x264' $Filename; then
            FilenameNew=$(echo $Filename|sed 's/264/265/')
            ffmpeg -hide_banner -i $FullPath -c:v libx265 -preset medium -crf 23 -c:a copy $FullPath$FilenameNew
        fi
        FilenameNew=${FileStub}.h265${FileExt}
        ffmpeg -i $FullPath -c:v libx265 -preset medium -crf 23 -c:a copy $FullPath$FilenameNew
    fi

done < <(find $PWD -type f \( -name '*.mp4' -o -name '*.mkv' \) -print0)

# list="$(find $CWD -name '*.mp4' -o -name '*.mkv')"

# for file in list; do
# printf '%s\n' "$file"
# done

# docker run --volume /mnt/data/Davide:/tmp/workdir jrottenberg/ffmpeg:4.0 -stats \
#    -i Film/Altrimenti\ ci\ arrabbiamo\!.mkv -map_metadata 0 -c:v libx265 -preset medium -crf 20 -c:a copy -c:s copy -map 0 completed/Altrimenti\ ci\ arrabbiamo\!.mkv
